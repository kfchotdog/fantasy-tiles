import React from 'react';
import {
  Image
} from 'react-native';
import { Actions, Scene } from 'react-native-router-flux';
import Main from '../component/Main';
import Game from '../component/Game';
import colors from '../utils/colors';

export default Actions.create(
  <Scene
    key="root"
    navigationBarStyle={{ backgroundColor: colors.blue }}
    titleStyle={{ color: colors.white }}
  >
    <Scene
      key="main"
      initial
      component={Main}
      title='Main'
    />
    <Scene
      key="game"
      component={Game}
      title='Game'
    />
  </Scene>,
);
