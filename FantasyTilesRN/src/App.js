import React, { Component } from 'react';
import { Router } from 'react-native-router-flux';
import AppRoutes from './configuration/configRouter';

export default class PlaceSearcher extends Component {
  render() {
    return (
      <Router scenes={AppRoutes} />
    );
  }
}
