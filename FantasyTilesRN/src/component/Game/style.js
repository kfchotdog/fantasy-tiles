import {
  StyleSheet
} from 'react-native';
import sizes from '../../utils/sizes';

const styles = StyleSheet.create({
  container: {
    marginTop: sizes.navbarHeight,
    flexDirection: 'row',
    justifyContent:'center',
    alignItems: 'center',
    flexWrap:'wrap',
  },
});

export default styles;
