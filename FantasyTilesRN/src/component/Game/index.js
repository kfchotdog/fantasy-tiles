import React, { Component } from 'react';
import {
  ScrollView,
  View,
} from 'react-native';
import Tile from '../Tile';
import AddableTile from '../AddableTile';
import styles from './style';

export default class Game extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tileNum: props.tileNum,
    }
  }
  render() {
    const { tileNum } = this.state;
    const tiles = Array.from(new Array(tileNum),(val,index)=>index);;

    return (
      <ScrollView>
        <View style={styles.container}>
          {tiles.map((val) => {
            return (
              <Tile
                key={val}
                tileIndex={val+1}
                onLongPress={() => {
                  this.setState({
                    tileNum: tileNum - 1,
                  });
                }}
              />
            );
          })}
          <AddableTile onPress={() => {
            this.setState({
              tileNum: tileNum + 1,
            });
          }}/>
        </View>
      </ScrollView>
    );
  }
}
