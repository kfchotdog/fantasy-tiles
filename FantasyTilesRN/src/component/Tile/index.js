import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Animated,
} from 'react-native';
import styles from './style';

// got inspired and params by https://daneden.github.io/animate.css/
const rubberBand = {
  inputRange: [0, 0.3, 0.4, 0.5, 0.65, 0.75, 1],
  outputRangeForScaleX: [1, 1.25, 0.75, 1.15, 0.95, 1.05,  1],
  outputRangeForScaleY: [1, 0.75, 1.25, 0.85, 1.05, 0.95, 1],
};
const bounceOut = {
  inputRangeForOpacity: [0, 1],
  inputRangeForScale: [0, 0.2, 0.5, 0.55, 1],
  outputRangeForOpacity: [1, 0],
  outputRangeForScale: [1, 0.9, 1.11, 1.11, 0.3],
};

const defaultProps = {
  enableLongPress: true,
};

export default class Tile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      animatedValueForRobberBand: new Animated.Value(0),
      deleting: false,
      animatedValueForBounceOut: new Animated.Value(0),
    };

    this.startRobberBandAnimation = this.startRobberBandAnimation.bind(this);
    this.handleTilePress = this.handleTilePress.bind(this);
    this.handleTileLongPress = this.handleTileLongPress.bind(this);
  }
  componentDidMount() {
    this.startRobberBandAnimation(500);
  }
  startRobberBandAnimation(delay) {
    Animated.timing(this.state.animatedValueForRobberBand, {
      toValue: 1,
      delay: delay,
    }).start(()=> {
      this.state.animatedValueForRobberBand.setValue(0);
    });
  }
  startBounceOutAnimation(callback) {
    if (!this.deleting) {
      this.setState({
        deleting: true,
      });

      Animated.timing(this.state.animatedValueForBounceOut, {
        toValue: 1,
      }).start(() => {
        this.state.animatedValueForBounceOut.setValue(0);
        this.setState({
          deleting: false,
        });
        callback();
      });
    }
  }
  handleTilePress() {
    this.startRobberBandAnimation(0);
    if (this.props.onPress) {
      this.props.onPress();
    }
  }
  handleTileLongPress() {
    const { enableLongPress } = this.props;

    if (enableLongPress) {
      this.startBounceOutAnimation(() => {
        if (this.props.onLongPress) {
          this.props.onLongPress();
        }
      });
    }
  }
  render() {
    const { tileIndex } = this.props;
    const { deleting } = this.state;

    const animatableStyleForRobberBand = {
      transform: [
        {
          scaleX: this.state.animatedValueForRobberBand.interpolate({
            inputRange: rubberBand.inputRange,
            outputRange: rubberBand.outputRangeForScaleY
          })
        },
        {
          scaleY: this.state.animatedValueForRobberBand.interpolate({
            inputRange: rubberBand.inputRange,
            outputRange: rubberBand.outputRangeForScaleX
          }),
        }
      ],
    };
    const animatableStyleForBounceOut = {
      transform: [
        {
          scale: this.state.animatedValueForBounceOut.interpolate({
            inputRange: bounceOut.inputRangeForScale,
            outputRange: bounceOut.outputRangeForScale
          }),
        }
      ]
    };

    const animatableStyleForOpacity = this.state.animatedValueForBounceOut.interpolate({
      inputRange: [0, 1],
      outputRange: [1, 0],
    });

    return (
      <View>
        <TouchableOpacity
          onPress={this.handleTilePress}
          onLongPress={this.handleTileLongPress}
        >
          <Animated.View
            style={[styles.container, animatableStyleForRobberBand, deleting ? animatableStyleForBounceOut : null, { opacity: animatableStyleForOpacity }]}
          >
            <Text style={styles.text}>
              {tileIndex}
            </Text>
          </Animated.View>
        </TouchableOpacity>
      </View>
    );
  }
}

Tile.defaultProps = defaultProps;
