import {
  StyleSheet
} from 'react-native';
import sizes from '../../utils/sizes';
import colors from '../../utils/colors';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: sizes.margin,
    width: sizes.screen.widthOneFifth,
    height: sizes.screen.widthOneFifth,
    backgroundColor: colors.blue,
    borderRadius: sizes.screen.widthOneFifth / 6,
  },
  text: {
    color: colors.white,
    fontWeight: 'bold',
    fontSize: 25,
  },
});

export default styles;
