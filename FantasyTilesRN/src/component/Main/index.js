import React, { Component } from 'react';
import {
  View,
  ListView,
  StatusBar,
} from 'react-native';
import PropTypes from 'prop-types';
import { Actions } from 'react-native-router-flux';
import styles from './style';
import colors from '../../utils/colors';
import TileNumberGroup from './TileNumberGroup';

export default class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tileNum: 5,
    };
  }
  render() {
    const { tileNum } = this.state;

    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={colors.blue}
          barStyle="light-content"
        />
        <TileNumberGroup
          tileNumber={tileNum}
          onChange={(tileNum) => {
            this.setState({
              tileNum: tileNum
            })
          }}
          onPushTo={() => {
            Actions.game({tileNum: tileNum});
          }}
        />
      </View>
    );
  }
}
