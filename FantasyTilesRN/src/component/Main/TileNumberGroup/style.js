import {
  StyleSheet
} from 'react-native';
import colors from '../../../utils/colors';

const styles = StyleSheet.create({
  container: {
    zIndex: 1,
  },
  square: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: colors.lightgray,
    borderWidth: 3,
    borderRadius: 5,
    width: 300,
    height: 200,
    zIndex: 2,
  },
  infoText: {
    marginHorizontal: 10,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 25,
  },
  counterText: {
    fontSize: 30,
    margin: 15,
    textAlign: 'center',
    backgroundColor: 'transparent',
  },
  line: {
    height: 1,
    marginHorizontal: 10,
    backgroundColor: colors.lightgray,
    marginTop: 5,
  },
  mainContainer: {
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  gameOnBtn: {
    height: 40,
    backgroundColor: colors.blue,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
  },
  gameOnText: {
    fontSize: 22,
    textAlign: 'center',
    color: colors.white,
    backgroundColor: 'transparent',
  }
});

export default styles;
