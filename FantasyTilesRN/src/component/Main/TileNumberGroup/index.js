import React, { Component } from 'react';
import {
  View,
  Dimensions,
  TouchableOpacity,
  TextInput,
  Text,
  Animated,
} from 'react-native';
import styles from './style';

const hitSlop = {top:10, bottom: 10, left: 10, right: 10};

export default class TileNumberGroup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      animatedScaleValue: new Animated.Value(1),
    };

    this.handleAnimationOnPressIn = this.handleAnimationOnPressIn.bind(this);
    this.handleAnimationOnPressOut = this.handleAnimationOnPressOut.bind(this);
  }
  handleAnimationOnPressIn() {
    Animated.spring(this.state.animatedScaleValue, {
      toValue: 2,
    }).start();
  }
  handleAnimationOnPressOut() {
    Animated.spring(this.state.animatedScaleValue, {
      toValue: 1,
    }).start();
  }
  render() {
    const { onChange, onPushTo, tileNumber } = this.props;
    const animatedScale = {
      transform: [{ scale: this.state.animatedScaleValue }],
    }

    return (
      <View style={styles.container}>
        <View
          style={styles.square}
        >
          <View>
            <Text style={styles.infoText}>
              Initial Tile Number:
            </Text>
            <View style={styles.line} />
            <View style={styles.mainContainer}>
              <TouchableOpacity
                style={styles.minusContainer}
                hitSlop={hitSlop}
                onPress={() => {
                  onChange(tileNumber > 1 ? tileNumber - 1 : tileNumber);
                }}
                onPressIn={this.handleAnimationOnPressIn}
                onPressOut={this.handleAnimationOnPressOut}
              >
                <Text style={styles.counterText}>
                  -
                </Text>
              </TouchableOpacity>
              <Animated.Text style={ [styles.counterText, animatedScale] }>
                {this.props.tileNumber}
              </Animated.Text>
              <TouchableOpacity
                style={styles.addContainer}
                hitSlop={hitSlop}
                onPress={() => {
                  onChange(tileNumber > 9 ? tileNumber : tileNumber + 1);
                }}
                onPressIn={this.handleAnimationOnPressIn}
                onPressOut={this.handleAnimationOnPressOut}
              >
                <Text style={styles.counterText}>
                  +
                </Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={styles.gameOnBtn}
              onPress={() => {
                onPushTo();
              }}
            >
              <Text
                style={styles.gameOnText}
              >
                Game On
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
