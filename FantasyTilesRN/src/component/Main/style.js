import {
  StyleSheet,
} from 'react-native';
import sizes from '../../utils/sizes';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: sizes.navbarHeight,
  },
});

export default styles;
