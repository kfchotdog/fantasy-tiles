import React, { Component } from 'react';
import Tile from '../Tile';

export default class AddableTile extends Component {
  render() {
    return (
      <Tile
        {...this.props}
        tileIndex={'+'}
        enableLongPress={false}
      />
    );
  }
}
