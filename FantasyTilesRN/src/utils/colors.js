export default {
  blue: '#30B2FE',
  white: '#FFFFFF',
  gray: '#0099FF',
  lightgray: '#EDEDED',
};
