import {
  Dimensions,
  Platform,
} from 'react-native';

const { width, height } = Dimensions.get('window');
const screenHeight = width < height ? height : width;
const screenWidth = width < height ? width : height;

export default {
  // Window Dimensions
  screen: {
    height: screenHeight,
    width: screenWidth,

    widthHalf: screenWidth * 0.5,
    widthThird: screenWidth * 0.333,
    widthTwoThirds: screenWidth * 0.666,
    widthQuarter: screenWidth * 0.25,
    widthThreeQuarters: screenWidth * 0.75,
    widthOneFifth: screenWidth * 0.2,
  },
  navbarHeight: (Platform.OS === 'ios') ? 64 : 54,
  basicNavbarHeight: (Platform.OS === 'ios') ? 24 : 14,

  margin: 20,
  margingSml: 10,
  padding: 20,
  paddingSml: 10,
  borderRadius: 10,
};
