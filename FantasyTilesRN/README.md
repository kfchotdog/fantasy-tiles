# FantasyTiles-ReactNative

> ### React Native + React-Native-Router-Flux codebase containing fantasy tile implementation.

Some animations are inspired by [Animated.css](https://daneden.github.io/animate.css/).

##Demo Gifs
![](https://media.giphy.com/media/xUA7b8L0Dvt6VG1RUA/giphy.gif)

## Getting started

###Installation

####Install React-Native

Follow [official doc](https://facebook.github.io/react-native/docs/getting-started.html) to install React Native evironment.

####Install project dependencies

Install project dependencies with [yarn](https://yarnpkg.com/lang/en/docs/install/) or [npm](https://www.npmjs.com/get-npm)

```sh
$ yarn install
# or
$ npm install
```

####Running the React Native application 

#####iOS
```sh
$ react-native run-ios
```

#####Android
```sh
$ react-native run-android
```